**Installation**  
**1.** add this to composer.json
```
    "repositories": [  
        {"type": "composer", "url": "https://pelkas.repo.repman.io"}  
    ]  
```
**2.** composer require BannerBundle  
**3.** bin/console coaladmin:create-banner-bundle  
**4.** bin/console assets:install
