import React, {useEffect, useState} from 'react';

import SectionForm from "./SectionForm";
import Button from "../../../assets/App/components/multi_use_components/Button";
import SectionTile from "../../../assets/App/components/multi_use_components/SectionTile";
import Icon from "./icon.png";
import SectionExampleImage from "../../../assets/App/components/multi_use_components/SectionExampleImage";
import ExamplePicture from "./ExamplePicture.png";
import SectionFormBase from "../../../assets/App/components/base_components/SectionFormBase";

const BannerBundleForm = ({currentMenuItem,sectionList, pagesList}) => {
    const [showForm, setShowForm] = useState(false)
    const [showExampleImage, setShowExampleImage] = useState(false)
    const [title, setTitle]= useState('Banner')

    return (
        <>
            <SectionTile
                title={title}
                icon={Icon}
                description={'Lorem ipsum ...'}
                showExampleImgFunction={e=>setShowExampleImage(true)}
                showSectionFormFunction={e=>setShowForm(true)}
            />
            {showExampleImage === true ? <SectionExampleImage img={ExamplePicture} closeFunction={e=>setShowExampleImage(false)} /> : null }
            {showForm === true ? <SectionFormBase closeBtnFucntion={e=>setShowForm(false)}
                                                  title={title}
                                                  formContainerClass={''}
                                                  form={<SectionForm item={null}
                                                                     sectionList={sectionList}
                                                                     currentMenuItem={currentMenuItem}
                                                                     pagesList={pagesList}
                                                                     bundleName={'BannerBundle'}
                                                  />}
            /> : null }
        </>



        // <div>
        //     <Button onClick={(e) => {
        //         setShowForm(!showForm)
        //     }} text={'Dodaj komponent'} btnClassName={'jakas'}/>
        //     {showForm === false ? null : <>
        //         <SectionForm closeBtnFunction={(e) => setShowForm(false)}
        //                                               currentMenuItem={currentMenuItem}
        //                                               menuItem={null}
        //                                               sectionList={sectionList}
        //                                               bundleName={bundleName}
        //                                               pagesList={pagesList}
        //
        //     /></>}
        // </div>
    )
}
export default BannerBundleForm
