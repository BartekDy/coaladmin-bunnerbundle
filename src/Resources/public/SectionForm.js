import React, {useEffect, useRef, useState} from 'react';
import {useForm} from "react-hook-form";
import FormBase from "../../../assets/App/components/base_components/FormBase";

import '../../FileUploader/dist/jquery.fileuploader.min'
import Fileuploader from "../../FileUploader/src/thirdparty/react/Fileuploader";


const SectionForm = ({closeBtnFunction, menuItem, currentMenuItem,sectionList,bundleName,pagesList}) => {
    const [showForm, setShowForm] = useState(false)
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [fields, setFields] = useState(null);


    const {register, control, formState: {errors}} = useForm();
    const [title, setTitle] = useState(menuItem?.title ?? '');
    const [description, setDescription] = useState(menuItem?.description ?? '');
    const [images, setImages] = useState(menuItem?.images ?? '');
    const [buttonText, setButtonText] = useState(menuItem?.buttonText ?? '');
    const form = useRef(null);


    const [send, setSend] = useState(null);
    const [pageSectionId, setPageSectionId] = useState(null)
    const resource = '/api/page_sections';

    const currentSection = sectionList.find(el => el.title === bundleName);
    const currentPage = pagesList.find((el)=>{
       var array=el.menuItem.split('/')
        if(array[array.length - 1] == currentMenuItem){
            return el.id
        }
    })
    const onSubmit = (e) => {
        e.preventDefault()
        var formData = new FormData(e.target);
        const dataToSend = {
            page: `/api/pages/${currentPage.id}`,
            section: `/api/sections/${currentSection.id}`,
            params: [
                {
                    elements: [
                        {
                            id: 1,
                            name: "Naglowek",
                            value: title,
                            htmlTag: "h1",
                            inputType: "text"
                        },
                        {
                            id: 2,
                            name: "Opis",
                            value: description,
                            htmlTag: "p",
                            inputType: "textarea"
                        },
                        {
                            id: 3,
                            name: "Text buttonu",
                            value: buttonText,
                            htmlTag: "none",
                            inputType: "props"
                        },
                        {
                            img: null
                        }
                    ]
                }
            ],
        }

        fetch(`${window.location.origin}/api/page_sections`, {
            method: 'POST',
            body: JSON.stringify(dataToSend),
            headers: {
                'Content-Type': 'application/json'
            },
        })
            .then((response) => {
                return response.json()
            })
            .then(data => {
                formData.append('pageSectionId', data.id);
                formData.append('path', 'uploads/Banner/');
                fetch(`${window.location.origin}/api/uploadImageApi`, {
                    method: 'POST',
                    body: formData,
                })
                    .then((response) => {
                        response.status === 201 ? document.getElementsByClassName('close_btn')[0].click() : null
                        window.location.reload(true)
                        return response.json()
                    })
                    .then(data => {
                    })
                    .catch((error) => {
                    });
            })
            .catch((error) => {
            });
    }


    return (

                      <form method={'POST'} ref={form} className='add_section_text_with_image' onSubmit={onSubmit}>
                          <div className="form-control">
                              <label>Header sekcji</label>
                              <input type="text" name="title" {...register("title")}
                                     value={title} onChange={(e) => setTitle(e.target.value)}
                              />
                          </div>
                          <div className="form-control">
                              <label>Opis</label>
                              <input type="textarea" name="title" {...register("description")}
                                     value={description} onChange={(e) => setDescription(e.target.value)}
                              />
                          </div>
                              <label>Zdjęcie</label>
                              <Fileuploader name="files"  limit="5" addMore={'addMore'} sortable={true} extensions="image/*" captions='pl' theme={'thumbnails'}/>
                          <div className="form-control">
                              <label>Text w buttonie</label>
                              <input type="text" name="title" {...register("buttonText")}
                                     value={buttonText} onChange={(e) => setButtonText(e.target.value)}
                              />
                          </div>
                          <button id="submit" type="submit" className="btn btn-block"><i
                              className="far fa-save"> </i> Zapisz
                          </button>
                      </form>

    )
}
export default SectionForm;
