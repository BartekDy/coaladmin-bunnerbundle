import React, {useEffect, useState} from 'react';
import ComponentMenu from "../../../assets/App/components/multi_use_components/ComponentMenu";
import EditForm from "./EditForm";
import SectionFormBase from "../../../assets/App/components/base_components/SectionFormBase";


const BannerBundle = ({params, id, components}) => {
    const [heading, setHeading] = useState(null)
    const [description, setDescription] = useState(null)
    const [buttonText, setButtonText] = useState(null)
    const [imageArray, setImageArray] = useState(null)
    const [showEditForm, setShowEditForm] = useState(false)

    useEffect(() => {
        params[0].elements.map((el=> {
            if(el.img){
                const pathArray = []
                el.img.map((image =>{
                   pathArray.push(image.src)
                }))
                setImageArray(pathArray);
            }
                    switch (el.name) {
                        case 'Naglowek':
                            return setHeading(el.value)
                        case 'Opis':
                            return setDescription(el.value)
                        case 'Text buttonu':
                            return setButtonText(el.value)
                        default:
                            return null
                    }
        }))
    },[])

    return (
         heading && description && buttonText && imageArray !== null ?
             <>
             <section className={'BannerBundle'}>
                 <div>
                     <h1>{heading}</h1>
                     <p>{description}</p>
                     <button>{buttonText}</button>
                 </div>
                 <div>
                     {imageArray.map((el, index)=>{
                       return  <img style={{width: "100px", hight: "100px"}} src={`${window.location.origin}/${el}`} alt={''} key={index} />
                     })}
                 </div>
                 <ComponentMenu id={id} components={components} editFunction={e=>setShowEditForm(true)}/>
             </section>

                 {showEditForm === true ? <SectionFormBase closeBtnFucntion={e => setShowEditForm(false)}
                                                           title={'FiveColumnsThreeWithHeaderAndTextBundle'}
                                                           formContainerClass={''}
                                                           form={<EditForm id={id}/>} /> : null }
             </>
             : null
    )
}
export default BannerBundle;
