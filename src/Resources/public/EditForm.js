import React, {useRef, useState, useEffect} from 'react';
import {useForm} from "react-hook-form";
import Fileuploader from "../../FileUploader/src/thirdparty/react/Fileuploader";
import EditSection from "../../../assets/js/EditSection";

const EditForm = ({id}) =>{

    const [data, setData] = useState(null)
    const [showForm, setShowForm] = useState(false)
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [fields, setFields] = useState(null);


    const {register, control, formState: {errors}} = useForm();
    const [title, setTitle] = useState(null);
    const [description, setDescription] = useState(null);
    const [btnText, setBtnText] = useState(null);
    const [imgSrc, setImgSrc] = useState(null);
    const [images, setImages] = useState(null);




    const form = useRef(null);


    useEffect(() => {
        fetch(`${window.location.origin}/api/page_sections/${id}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            },
        })
            .then((response) => {
                return response.json()
            })
            .then(data => {
                data.params[0].elements.map((el=> {
                    switch (el.name) {
                        case 'Naglowek':
                            return setTitle(el.value)
                        case 'Opis':
                            return setDescription(el.value)
                        case 'Text buttonu':
                            return setBtnText(el.value)
                        case 'img':
                            return setImgSrc(el)
                        default:
                            return null
                    }
                }))
            })
            .catch((error) => {
            });

        fetch(`${window.location.origin}/api/page_section_images`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            },
        })
            .then((response) => {
                return response.json()
            })
            .then(data => {

            let result = [];
                data["hydra:member"].map((el=>{
                let pageSectionArr = el.pageSection.split("/")
                if(pageSectionArr[pageSectionArr.length -1] == id ){
                    result.push(el)
                }
            }))
                setImages(result.sort(function (a,b){return a.position > b.position}))
            })
            .catch((error) => {
            })
    },[])

    const onSubmit = (e) => {
        e.preventDefault()

        var formData = new FormData(e.target)
        formData.append('pageSectionId', id);
        formData.append('path', 'uploads/Banner/');

        const data = [
            {
                elements: [
                    {
                        id: 1,
                        name: "Naglowek",
                        value: title,
                        htmlTag: "h1",
                        inputType: "text"
                    },
                    {
                        id: 2,
                        name: "Opis",
                        value: description,
                        htmlTag: "p",
                        inputType: "textarea"
                    },
                    {
                        id: 3,
                        name: "Text buttonu",
                        value: btnText,
                        htmlTag: "none",
                        inputType: "props"
                    },
                    {
                        img: imgSrc
                    }
                ]
            }
        ]

        EditSection(id,data, true, formData);
    }

    return(
         images !== null ? <form method={'POST'} ref={form} className='add_section_text_with_image' onSubmit={onSubmit}>
            <div className="form-control">
                <label>Header sekcji</label>
                <input type="text" name="title" {...register("title")}
                       value={title} onChange={(e) => setTitle(e.target.value)}
                />
            </div>
            <div className="form-control">
                <label>Opis</label>
                <input type="textarea" name="description" {...register("description")}
                       value={description} onChange={(e) => setDescription(e.target.value)}
                />
            </div>
            <label>Zdjęcie</label>
            <Fileuploader name="files" files={images}  limit="5" addMore={'addMore'} sortable={true} extensions="image/*" captions='pl' theme={'thumbnails'}/>
            <div className="form-control">
                <label>Text w buttonie</label>
                <input type="text" name="btnText" {...register("btnText")}
                       value={btnText} onChange={(e) => setBtnText(e.target.value)}
                />
            </div>
            <button id="submit" type="submit" className="btn btn-block"><i
                className="far fa-save"> </i> Zapisz
            </button>
        </form> : null

    )
}
export default EditForm;