<?php

namespace BannerBundle;

use BannerBundle\DependencyInjection\BannerBundleExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class BannerBundle extends Bundle
{
    /**
     * Overridden to allow for the custom extension alias.
     */
    public function getContainerExtension()
    {
        if (null === $this->extension) {
            $this->extension = new BannerBundleExtension();
        }
        return $this->extension;
    }
}