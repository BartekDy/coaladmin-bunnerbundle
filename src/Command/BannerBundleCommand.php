<?php
namespace BannerBundle\Command;

use App\Entity\Section;
use App\Repository\SectionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BannerBundleCommand extends Command
{
    private $em;
    private $sectionRepository;

    public function __construct(EntityManagerInterface $em, SectionRepository $sectionRepository)
    {
        $this->em = $em;
        $this->sectionRepository = $sectionRepository;

        parent::__construct();
    }

    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'coaladmin:create-banner-bundle';

    protected function configure(): void
    {
        $this
            ->setDescription('Creates a new banner.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $section = $this->sectionRepository->findOneBy(['title' => 'BannerBundle']);

        if (!$section) {
            $section = new Section();
        }

        $section
            ->setTitle('BannerBundle')
            ->setParams($this->getParams())
            ->setTemplateUrl('@CBannerBundle/banner_bundle/show.html.twig')
        ;

        $this->em->persist($section);
        $this->em->flush();

        return Command::SUCCESS;
    }

    private function getParams()
    {
        $params = [
            'elements'=> [
                [
                    "id" => 1,
                    "name" => "Nagłówek",
                    "value" => null,
                    "htmlTag" => "h1",
                    "inputType" => "text"
                ],
                [
                    "id" => 2,
                    "name" => "Opis",
                    "value" => null,
                    "htmlTag" => "p",
                    "inputType" => "textarea"
                ],
                [
                    "id" => 3,
                    "name" => "Text buttonu",
                    "value" => null,
                    "htmlTag" => "none",
                    "inputType" => "props"
                ],
                [
                    "img" => [
                        [
                            "id" => 4,
                            "htmlTag" => "img",
                            "safeName" => null
                        ],
                        [
                            "id" => 5,
                            "htmlTag" => "img",
                            "safeName" => null
                        ],
                        [
                            "id" => 6,
                            "htmlTag" => "img",
                            "safeName" => null
                        ],
                         [
                            "id" => 6,
                            "htmlTag" => "img",
                            "safeName" => null
                        ]
                    ]
                ]
            ]
        ];

        return $params;
    }
    private function getProps()
    {
        $props = [
            [
                "prop" => "currentMenuItem"
            ],
            [
                "prop" => "sectionList"
            ],
            [
                "prop" => "pagesList"
            ]
        ];

        return $props;
    }
}